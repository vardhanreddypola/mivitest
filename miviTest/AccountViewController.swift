//
//  AccountViewController.swift
//  miviTest
//
//  Created by Vardhan Pola on 04/08/18.
//  Copyright © 2018 Vardhan Pola. All rights reserved.
//

import UIKit

class AccountViewController: UIViewController {

    let includesArray  = NSMutableArray()
    let subscriptionsAttributes = NSMutableArray()
    let servicesAttributes = NSMutableArray()
    let productsAttributes = NSMutableArray()
     var userData = UserInfo()
    
    
    @IBOutlet weak var nameLabel: UILabel!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userData = UserInfo()
        print(userData.collectionResult.data.attributes as Any)
        print(userData.collectionResult.included as Any)
        
        nameLabel.text = userData.collectionResult.data.attributes.title + " " + userData.collectionResult.data.attributes.firstName + userData.collectionResult.data.attributes.lastName + "\n" + userData.collectionResult.data.attributes.emailAddress
        
        for includeObjData in userData.collectionResult.included {
            includesArray.add(includeObjData)
            
//            servicesAttributes.add((includesArray[0] as! Included).attributes)
//            subscriptionsAttributes.add((includesArray[1] as! Included).attributes)
//            productsAttributes.add((includesArray[2] as! Included).attributes)
            
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func logout(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


class UserInfoCell: UITableViewCell {
    
    @IBOutlet weak var infoLabel:UILabel!

}


extension AccountViewController:UITableViewDelegate,UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
       return userData.collectionResult.included.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
     
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UserInfoCell
     
        let attributes = userData.collectionResult.included[indexPath.section].attributes
        if indexPath.section == 0{

            
            
            cell.infoLabel.text = "msn : \(String(describing: attributes.msn!))" + "\n \n" + "credit : \(String(describing: attributes.credit!))" + "\n \n" + "credit-expiry : \(String(describing: attributes.creditExpiry!))" + "\n \n" + "data-usage-threshold : \(String(describing: attributes.dataUsageThreshold!))"
            
            
        }else if indexPath.section == 1{
           
            cell.infoLabel.text = "included-data-balance : \(String(describing: attributes.includedDataBalance))" + "\n \n" + "included-credit-balance : \(String(describing: attributes.includedCreditBalance))" + "\n \n" + "included-rollover-credit-balance : \(String(describing: attributes.includedRolloverCreditBalance))" + "\n \n" + "included-rollover-data-balance : \(String(describing: attributes.includedRolloverDataBalance))" + "\n \n" + "included-international-talk-balance : \(String(describing: attributes.includedInternationalTalkBalance))" + "\n \n" + "expiry-date \(String(describing: attributes.expiryDate))" + "\n \n" + "auto-renewal : \(String(describing: attributes.autoRenewal))" + "\n \n" + "primary-subscription :\(String(describing: attributes.primarySubscription))"
            
            
        }else{
        
            cell.infoLabel.text = "name : \(String(describing: attributes.name))" + "\n \n" + "included-data : \(String(describing: attributes.includedDataBalance))" + "\n \n" + "included-credit : \(String(describing: attributes.includedCredit))" + "\n \n" + "included-rollover-data-balance : \(String(describing: attributes.includedRolloverDataBalance))" + "\n \n" + "included-international-talk : \(String(describing: attributes.includedInternationalTalk))" + "\n \n" + "unlimited-text \(String(describing: attributes.unlimitedText))" + "\n \n" + "unlimited-talk : \(String(describing: attributes.unlimitedTalk))" + "\n \n" + "unlimited-international-text :\(String(describing: attributes.unlimitedInternationalText))" + "\n \n" + "unlimited-international-talk :\(String(describing: attributes.unlimitedInternationalTalk))" + "\n \n" +  "price :\(String(describing: attributes.price))"
            
            
        }

        return cell
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let rowData = includesArray.object(at: section) as! Included
        
        return rowData.type
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }

}
