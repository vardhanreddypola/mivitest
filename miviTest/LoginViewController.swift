//
//  ViewController.swift
//  miviTest
//
//  Created by Vardhan Pola on 04/08/18.
//  Copyright © 2018 Vardhan Pola. All rights reserved.
//

import UIKit


class LoginViewController: UIViewController {
    @IBOutlet weak var emailTF:UITextField!
    @IBOutlet weak var loginBtn:UIButton!
    var collectionResult :Collection!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        loginBtn.addTarget(self, action: #selector(loginBtnAction), for: .touchUpInside)
        //        loginBtn.layer.cornerRadius = loginBtn.frame.width/2
        loginBtn.dropShadow()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func alertView(title Title: String, andAlertViewMessage message: String, buttonOkTitle okButton: String, buttonAction: @escaping () -> Void, andCancelButtonTitle cancebuttonTitle: String, withcancelButton cancelButtonAction: @escaping () -> Void, with currentViewController: UIViewController) {
        
        let alertController = UIAlertController(title: Title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: okButton, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            buttonAction()
        }))
        if cancebuttonTitle != "" {
            alertController.addAction(UIAlertAction(title: cancebuttonTitle, style: .default, handler: {(_ action: UIAlertAction) -> Void in
                cancelButtonAction()
            }))
        }
        currentViewController.present(alertController, animated: true) {
            
        }
    }
    
}
extension UIView {
    
    // OUTPUT 1
    func dropShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: -1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

extension LoginViewController{
    
    @objc func loginBtnAction()  {
        
        if (emailTF.text?.count)! > 0 {
            
            var userData = UserInfo()
            let emailFromJson = userData.collectionResult.data.attributes.emailAddress
            
            if emailTF.text == emailFromJson {
                print("Login Success")
                
                
                alertView(title: "MIVI", andAlertViewMessage: "Login Success", buttonOkTitle: "Okay", buttonAction: {
                    let accountVC = self.storyboard?.instantiateViewController(withIdentifier: "AccountViewController") as! AccountViewController
                    self.navigationController?.pushViewController(accountVC, animated: true)
                }, andCancelButtonTitle: "", withcancelButton: {
                    
                }, with: self)

            }else{
                alertView(title: "MIVI", andAlertViewMessage: "Enter Valid Email", buttonOkTitle: "Okay", buttonAction: {
                    
                }, andCancelButtonTitle: "", withcancelButton: {
                    
                }, with: self)
            }
        }else{
            print("Please enter email")
            
            alertView(title: "MIVI", andAlertViewMessage: "Please enter email", buttonOkTitle: "Okay", buttonAction: {
                
                
            }, andCancelButtonTitle: "", withcancelButton: {
                
            }, with: self)
 
        }
    }
}
